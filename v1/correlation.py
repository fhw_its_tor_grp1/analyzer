import sys
from datetime import datetime, timedelta
import pyshark as ps
import numpy as np
import scipy.stats as stats

#usage: python correlation.py file1.pcap file2.pcap timeslotspan

#class used to store the L- and S-vector of each file
class StatData:
    def __init__(self):
        self.size_v = np.zeros(1, dtype=np.int)
        self.count_v = np.zeros(1, dtype=np.int)

#takes two data object and equalizes the length of the vectors stored within them
#to do this the shorter array is padded with cells containing 0
def equalize_length(data_a, data_b):
    dif = data_b.size_v.shape[0] - data_a.size_v.shape[0]
    lengthen = []
    if (dif < 0):
        lengthen = data_b
    if (dif > 0):
        lengthen = data_a
    if (lengthen):
        lengthen.count_v = np.pad(lengthen.count_v, (0, abs(dif)), 'constant')  
        lengthen.size_v = np.pad(lengthen.size_v, (0, abs(dif)), 'constant')

#gets end time for the first timeslot. First time slot ends at the capture time of the first packet + SPAN
def get_initial_time_slot(filename_a, filename_b):
    global SPAN
    file_a = ps.FileCapture(filename_a)
    time_a = file_a[0].sniff_time + SPAN
    file_a.close()
    file_b = ps.FileCapture(filename_b)
    time_b = file_b[0].sniff_time + SPAN
    file_b.close()
    if (time_a < time_b):
        return time_a
    else:
        return time_b

#takes the given packet and the relevant data object and calls the actual callback function   
def vectorCallback_1(*args):
    global data_1
    vectorCallback(data_1, args[0])

def vectorCallback_2(*args):
    global data_2
    vectorCallback(data_2, args[0])

def vectorCallback(data, pkt):
    #building the vectors used for computing the correlation
    #everything relating to count_v concerns building the L-vector
    #everything relating to size_v concerns building the S-vector
    global cur_slot
    global SPAN
    pkt_size = int(pkt.ip.len) - int(pkt.ip.hdr_len)#TODO eventuell pkt.tcp verwenden?
    #initializing next time slot, if packet is outside the current slot
    while (pkt.sniff_time > cur_slot):
        data.count_v = np.append(data.count_v, 0) #TODO append on numpy arrays is rather inefficient, as the entire array has to be copied, but given that we probably won't analyze for long I deemed this feasible. However, while feasible these append calls should be replaced with pad to prevent unnecessary calls, if there's multiple empty and adjacent timeslots.
        data.size_v = np.append(data.size_v, 0)
        cur_slot = cur_slot + SPAN 
   #updating values
    idx = data.count_v.shape[0] - 1
    data.count_v[idx] = data.count_v[idx] + 1
    data.size_v[idx] = data.size_v[idx] + pkt_size

#span of a timeslot in seconds
SPAN = timedelta(seconds=float(sys.argv[3]))
#filenames
filename_1 = sys.argv[1]
filename_2 = sys.argv[2]
#filename_1 = 'pyshark_test_1.pcap'
#filename_2 = 'pyshark_test_2.pcap'
#data objects
data_1 = StatData()
data_2 = StatData()
#first time slot, as in SPAN seconds after the first recorded packet
initial_slot = get_initial_time_slot(filename_1, filename_2)

#TODO add argv param for 'use_json'=argv to possibly support that filetype as well
cap_1 = ps.FileCapture(filename_1)

#initialising current slot with initial slot
cur_slot = initial_slot
#building vectors for first capture file
cap_1.apply_on_packets(vectorCallback_1)

#resetting current slot
cur_slot = initial_slot
#building vectors for second capture file
cap_2 = ps.FileCapture(filename_2)
cap_2.apply_on_packets(vectorCallback_2)
#making sure the vectors have the same length so the stats functions don't crash
equalize_length(data_1, data_2)

#Computing correlation with pearson/spearman method
pearson_l = stats.pearsonr(data_1.count_v, data_2.count_v)[0]
pearson_s = stats.pearsonr(data_1.size_v, data_2.size_v)[0]
#TODO spearman is just for shits and giggles for now - have to look into spearman to make sure this usage is correct
spearman_l = stats.spearmanr(data_1.count_v, data_2.count_v)[0]
spearman_s = stats.spearmanr(data_1.size_v, data_2.size_v)[0]
#printing data
print("%s,%s,%s,%f,%f,%f,%f" % (filename_1, filename_2, SPAN, pearson_l, pearson_s, spearman_l, spearman_s))



