module gitlab.com/fhw_its_tor_grp1/analyzer/v3

go 1.15

require (
	github.com/aclements/go-moremath v0.0.0-20201123183122-222cfcba2589 // indirect
	github.com/dgryski/go-onlinestats v0.0.0-20170612111826-1c7d19468768
	github.com/google/gopacket v1.1.19
	github.com/spf13/viper v1.7.1
)
