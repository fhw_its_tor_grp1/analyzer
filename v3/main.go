package main

import (
	"encoding/json"
	"fmt"
	gs "github.com/dgryski/go-onlinestats"
	"github.com/spf13/viper"
	"gitlab.com/fhw_its_tor_grp1/analyzer/v3/analyzer"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Files struct {
	Svc []string `json:"service"`
	Cli []string `json:"client"`
}

type Config struct {
	Files  Files               `json:"files"`
	Config []map[string]string `json:"config"`
}

func generateTimespans() []time.Duration {
	min := viper.GetInt("timespans.min")
	max := viper.GetInt("timespans.max")
	step := viper.GetInt("timespans.step")

	count := (max-min)/step + 1
	timespans := make([]time.Duration, count)

	for i := 0; i < count; i++ {
		timespans[i] = time.Duration((min + i*step)) * time.Second
	}

	return timespans
}

func main() {
	// basePath := "C:\\Users\\mapthegod\\Desktop\\20201209"
	var wg sync.WaitGroup

	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath(".")
	if nil != viper.ReadInConfig() {
		panic(fmt.Errorf("Error in config file"))
	}

	basePath := viper.GetString("basePath")
	files, err := ioutil.ReadDir(basePath)
	if err != nil {
		panic(err)
	}

	count := len(files)
	timespans := generateTimespans()
	pad := viper.GetBool("pad")

	for i := 1; i <= count; i++ {
		confData, err := ioutil.ReadFile(filepath.Join(basePath, strconv.Itoa(i), "config.json"))
		if err != nil {
			panic(err)
		}
		var config Config
		err = json.Unmarshal(confData, &config)
		if err != nil {
			panic(err)
		}

		wg.Add(1)
		go runAnalysis(basePath, config, timespans, i, pad, &wg)

	}
	wg.Wait()
}

func runAnalysis(basePath string, config Config, timespans []time.Duration, iteration int, pad bool, wg *sync.WaitGroup) {
	defer wg.Done()
	svc := make([][]analyzer.PacketMeta, len(config.Files.Svc))
	cli := make([][]analyzer.PacketMeta, len(config.Files.Cli))

	for k, s := range config.Files.Svc {
		svc[k] = analyzer.ReadPacketMeta(filepath.Join(basePath, strconv.Itoa(iteration), s[strings.LastIndex(s, "/")+1:]))
	}

	for k, s := range config.Files.Cli {
		cli[k] = analyzer.ReadPacketMeta(filepath.Join(basePath, strconv.Itoa(iteration), s[strings.LastIndex(s, "/")+1:]))
	}

	for sid, s := range svc {
		for cid, c := range cli {
			printCorrelation(s, c, iteration, strconv.Itoa(sid+1), strconv.Itoa(cid+1), pad, timespans)
			//go printCorrelation(s, c, i, config.Files.Svc[sid][strings.LastIndex(config.Files.Svc[sid], "/")+1:], config.Files.Cli[cid][strings.LastIndex(config.Files.Cli[cid], "/")+1:], true)
		}
	}
}

func printCorrelation(p1 []analyzer.PacketMeta, p2 []analyzer.PacketMeta, iteration int, p1Name string, p2Name string, pad bool, timespans []time.Duration) {
	for _, tf := range timespans {
		l1, s1, l2, s2 := analyzer.MapBucketsToCorrelationVectors(analyzer.MapPacketsIntoBuckets(p1, p2, tf, pad))
		pearson_l := gs.Pearson(l1, l2)
		pearson_s := gs.Pearson(s1, s2)
		spearman_l, _ := gs.Spearman(l1, l2)
		spearman_s, _ := gs.Spearman(s1, s2)
		fmt.Printf("%02d::[%s|%s]{%04.1f} => %f %f %f %f\n", iteration, p1Name, p2Name, tf.Seconds(), pearson_l, pearson_s, spearman_l, spearman_s)
	}
}
