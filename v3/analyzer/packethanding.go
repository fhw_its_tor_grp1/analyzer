package analyzer

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"time"
)

type PacketMeta struct {
	Ts            time.Time
	FrameLength   int
	CaptureLength int
}

const ETH_IP_TCP_HEADER_SIZE = 66

func ReadPacketMeta(fileName string) []PacketMeta {
	handle, err := pcap.OpenOffline(fileName)
	defer handle.Close()
	if err != nil {
		panic(err)
	}
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	var metadata = make([]PacketMeta, len(packetSource.Packets()))

	for packet := range packetSource.Packets() {
		//fmt.Println("Meta:", packet.Metadata().Timestamp, packet.Metadata().CaptureLength, packet.Metadata().Length, packet.TransportLayer().LayerType(), len(packet.TransportLayer().LayerContents()))
		metadata = append(metadata, handlePacket(packet))
	}
	return metadata
}

func handlePacket(packet gopacket.Packet) PacketMeta {
	return PacketMeta{
		Ts:            packet.Metadata().Timestamp,
		FrameLength:   packet.Metadata().Length,
		CaptureLength: packet.Metadata().CaptureLength,
	}
}

func GetTimeRangeFromPacketsMeta(packets []PacketMeta) (time.Time, time.Time, time.Duration) {
	return packets[0].Ts, packets[len(packets)-1].Ts, packets[len(packets)-1].Ts.Sub(packets[0].Ts)
}

func FindAnalysisTimeRange(p1 []PacketMeta, p2 []PacketMeta, pad bool) (time.Time, time.Time, time.Duration) {
	s1, e1, _ := GetTimeRangeFromPacketsMeta(p1)
	s2, e2, _ := GetTimeRangeFromPacketsMeta(p2)

	var s, e = time.Time{}, time.Time{}

	if s1.After(s2) {
		//if pad {
		//	s = s2
		//} else {
		s = s1
		//}
	} else {
		//if pad {
		//	s = s1
		// } else {
		s = s2
		//}
	}

	if e1.After(e2) {
		if pad {
			e = e1
		} else {
			e = e2
		}
	} else {
		if pad {
			e = e2
		} else {
			e = e1
		}
	}
	return s, e, e.Sub(s)
}

func MapPacketsIntoBuckets(p1 []PacketMeta, p2 []PacketMeta, timeframe time.Duration, pad bool) ([][]PacketMeta, [][]PacketMeta) {
	s, _, d := FindAnalysisTimeRange(p1, p2, pad)
	d = d.Round(timeframe)

	pb1 := make([][]PacketMeta, int64(d.Milliseconds()/timeframe.Milliseconds())+1)
	pb2 := make([][]PacketMeta, int64(d.Milliseconds()/timeframe.Milliseconds())+1)
	for i := range pb1 {
		pb1[i] = make([]PacketMeta, 0)
		pb2[i] = make([]PacketMeta, 0)
	}
	for _, p := range p1 {
		idx := int64(p.Ts.Sub(s).Milliseconds() / timeframe.Milliseconds())
		if p.Ts.Before(s) {
			continue
		}
		pb1[idx] = append(pb1[idx], p)
	}
	for _, p := range p2 {
		idx := int64(p.Ts.Sub(s).Milliseconds() / timeframe.Milliseconds())
		if p.Ts.Before(s) {
			continue
		}
		pb2[idx] = append(pb2[idx], p)
	}

	return pb1, pb2
}

func MapBucketsToCorrelationVectors(p1 [][]PacketMeta, p2 [][]PacketMeta) ([]float64, []float64, []float64, []float64) {
	l1 := make([]float64, len(p1))
	s1 := make([]float64, len(p1))
	l2 := make([]float64, len(p1))
	s2 := make([]float64, len(p1))

	for i, pv := range p1 {
		for _, p := range pv {
			l1[i] += 1.0
			s1[i] += float64(p.FrameLength - ETH_IP_TCP_HEADER_SIZE)
		}
	}
	for i, pv := range p2 {
		for _, p := range pv {
			l2[i] += 1.0
			s2[i] += float64(p.FrameLength - ETH_IP_TCP_HEADER_SIZE)
		}
	}

	return l1, s1, l2, s2
}
